package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.MoneyHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyHistoryRepository extends JpaRepository<MoneyHistory, Long> {
    Page<MoneyHistory> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
