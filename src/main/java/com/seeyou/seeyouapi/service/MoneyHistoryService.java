package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.member.MemberItem;
import com.seeyou.seeyouapi.model.member.MemberSearchRequest;
import com.seeyou.seeyouapi.model.money.MoneyHistoryItem;
import com.seeyou.seeyouapi.model.money.MoneyHistoryRequest;
import com.seeyou.seeyouapi.model.money.MoneyHistoryResponse;
import com.seeyou.seeyouapi.model.money.MoneyHistorySearchRequest;
import com.seeyou.seeyouapi.repository.AttendanceRepository;
import com.seeyou.seeyouapi.repository.MoneyHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MoneyHistoryService {
    private final MoneyHistoryRepository moneyHistoryRepository;
    private final AttendanceRepository attendanceRepository;
    private final EntityManager entityManager;

    public void setMoneyDetails(Member member, MoneyHistoryRequest moneyHistoryRequest) {
        List<Attendance> originList = attendanceRepository.findAllByAttendanceYearAndAttendanceMonthAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), member);
        double totalMin = 0D;
        for (Attendance attendance : originList) {
            totalMin += Math.ceil(ChronoUnit.SECONDS.between(attendance.getTimeAttendance(), attendance.getTimeLeaveWork()) / 60.0);
        }

        // money = detailsRequest의 급여타입이 연봉일 때 내림으로 detailsRequest안에 있는 세전금액을 12로 나눔 :
        double money = moneyHistoryRequest.getMoneyType().equals(MoneyType.WAGE) ? Math.floor(moneyHistoryRequest.getBeforeMoney() / 12) : (moneyHistoryRequest.getBeforeMoney() / 60) * totalMin;
        MoneyHistory moneyHistory = new MoneyHistory.Builder(member, money, moneyHistoryRequest).build();
        moneyHistoryRepository.save(moneyHistory);
    }

    public ListResult<MoneyHistoryItem> getMoneyHistories(int page, MoneyHistorySearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<MoneyHistory> originList = getMoneyHistories(pageRequest, request);

        List<MoneyHistoryItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new MoneyHistoryItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<MoneyHistory> getMoneyHistories(Pageable pageable, MoneyHistorySearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MoneyHistory> criteriaQuery = criteriaBuilder.createQuery(MoneyHistory.class);

        Root<MoneyHistory> root = criteriaQuery.from(MoneyHistory.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getMoneyYear() != null) predicates.add(criteriaBuilder.equal(root.get("moneyYear"), request.getMoneyYear()));
        if (request.getMoneyMonth() != null) predicates.add(criteriaBuilder.equal(root.get("moneyMonth"), request.getMoneyMonth()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<MoneyHistory> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public MoneyHistoryResponse getMoneyHistory(long id) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }

    public ListResult<MoneyHistoryItem> getMyMoneyHistories(Member member) {
        List<MoneyHistory> originList = moneyHistoryRepository.findAll();
        List<MoneyHistoryItem> result = new LinkedList<>();
        for (MoneyHistory moneyHistory : originList) {
            result.add(new MoneyHistoryItem.Builder(moneyHistory).build());
        }
        return ListConvertService.settingResult(result);
    }

    public MoneyHistoryResponse getMyMoneyHistory(long id, Member member) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }
}