package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Basket;
import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.basket.BasketItem;
import com.seeyou.seeyouapi.model.basket.BasketRequest;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.product.ProductRequest;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderRequest;
import com.seeyou.seeyouapi.repository.BasketRepository;
import com.seeyou.seeyouapi.repository.ProductOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BasketService {
    private final BasketRepository basketRepository;
    private final ProductOrderRepository productOrderRepository;

    public void setBasket(Product product, BasketRequest request) {
        Basket basket = new Basket.Builder(product, request).build();
        basketRepository.save(basket);
    }

    public ListResult<BasketItem> getList() {
        List<Basket> originList = basketRepository.findAll();

        List<BasketItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new BasketItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

    public void putBasket(long id, BasketRequest request) {
        Basket basket = basketRepository.findById(id).orElseThrow(CMissingDataException::new);
        basket.putBasket(request);
        basketRepository.save(basket);
    }

    public void delBasket(long id) {
        basketRepository.deleteById(id);
    }

    public void delBasket() {
        List<Basket> originList = basketRepository.findAll();

        for (Basket basket : originList) {
            ProductOrderRequest request = new ProductOrderRequest();
            request.setQuantity(basket.getQuantity());
            ProductOrder productOrder = new ProductOrder.Builder(basket.getProduct(), request).build();
            productOrderRepository.save(productOrder);
        }

        basketRepository.deleteAll();
    }
}