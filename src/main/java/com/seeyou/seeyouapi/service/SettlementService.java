package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Settlement;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.settlement.SettlementItem;
import com.seeyou.seeyouapi.model.settlement.SettlementResponse;
import com.seeyou.seeyouapi.model.settlement.SettlementSearchRequest;
import com.seeyou.seeyouapi.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;
    private final EntityManager entityManager;

    public void setSettlement(BigDecimal price, String year, String month) {
        Settlement settlement = new Settlement.Builder(price, year, month).build();
        settlementRepository.save(settlement);
    }

    public ListResult<SettlementItem> getList(int page, SettlementSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Settlement> originList = getList(pageRequest, request);

        List<SettlementItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new SettlementItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Settlement> getList(Pageable pageable, SettlementSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Settlement> criteriaQuery = criteriaBuilder.createQuery(Settlement.class);

        Root<Settlement> root = criteriaQuery.from(Settlement.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getSettlementYear() != null) predicates.add(criteriaBuilder.equal(root.get("settlementYear"), request.getSettlementYear()));
        if (request.getSettlementMonth() != null) predicates.add(criteriaBuilder.equal(root.get("settlementMonth"), request.getSettlementMonth()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<Settlement> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public SettlementResponse getSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        BigDecimal price = BigDecimal.valueOf(0);
        for (Settlement settlement : originList) {
            price = price.add(settlement.getPrice());
        }
        return new SettlementResponse.Builder(price).build();
    }

    public void putSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        for (Settlement settlement : originList) {
            settlement.putIsComplete();
            settlementRepository.save(settlement);
        }
    }
}