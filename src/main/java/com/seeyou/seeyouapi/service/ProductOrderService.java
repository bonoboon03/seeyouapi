package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.member.MemberItem;
import com.seeyou.seeyouapi.model.member.MemberSearchRequest;
import com.seeyou.seeyouapi.model.productOrder.*;
import com.seeyou.seeyouapi.repository.ProductOrderRepository;
import com.seeyou.seeyouapi.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductOrderService {
    private final ProductOrderRepository productOrderRepository;
    private final EntityManager entityManager;

    public ProductOrder getProductData(long id) {
        return productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public ListResult<ProductOrderItem> getList(int page, ProductOrderSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<ProductOrder> originList = getList(pageRequest, request);

        List<ProductOrderItem> result = new LinkedList<>();


        originList.forEach(e -> result.add(new ProductOrderItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<ProductOrder> getList(Pageable pageable, ProductOrderSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductOrder> criteriaQuery = criteriaBuilder.createQuery(ProductOrder.class);

        Root<ProductOrder> root = criteriaQuery.from(ProductOrder.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getOrderYear() != null) predicates.add(criteriaBuilder.equal(root.get("orderYear"), request.getOrderYear()));
        if (request.getOrderMonth() != null) predicates.add(criteriaBuilder.equal(root.get("orderMonth"), request.getOrderMonth()));
        if (request.getOrderDay() != null) predicates.add(criteriaBuilder.equal(root.get("orderDay"), request.getOrderDay()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<ProductOrder> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public ProductOrderResponse getProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ProductOrderResponse.Builder(productOrder).build();
    }

    // 발주 상태 수정
    public void putProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        productOrder.putProductOrder();
        productOrderRepository.save(productOrder);
    }

    public ListResult<ProductOrderCompleteItem> getCompleteProductPageList(int page) {
        Page<ProductOrder> originData = productOrderRepository.findAllByIdGreaterThanEqualAndIsCompleteTrueOrderByCompleteDateOrderDesc(1, ListConvertService.getPageable(page));
        List<ProductOrderCompleteItem> result = new LinkedList<>();

        originData.forEach(e -> result.add(new ProductOrderCompleteItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }
}