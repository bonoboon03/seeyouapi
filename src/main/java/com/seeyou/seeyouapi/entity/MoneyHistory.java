package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.money.MoneyHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 4)
    private String moneyYear;

    @Column(nullable = false, length = 2)
    private String moneyMonth;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private BigDecimal beforeMoney;

    @Column(nullable = false)
    private BigDecimal nationalPension;

    @Column(nullable = false)
    private BigDecimal healthInsurance;

    @Column(nullable = false)
    private BigDecimal longTermCareInsurance;

    @Column(nullable = false)
    private BigDecimal employmentInsurance;

    @Column(nullable = false)
    private BigDecimal industrialAccidentInsurance;

    @Column(nullable = false)
    private BigDecimal incomeTax;

    @Column(nullable = false)
    private BigDecimal localIncomeTax;

    @Column(nullable = false)
    private BigDecimal money;

    private MoneyHistory(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.money = builder.money;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistory> {
        private final Member member;
        private final String moneyYear;
        private final String moneyMonth;
        private final MoneyType moneyType;
        private final BigDecimal beforeMoney;
        private final BigDecimal nationalPension;
        private final BigDecimal healthInsurance;
        private final BigDecimal longTermCareInsurance;
        private final BigDecimal employmentInsurance;
        private final BigDecimal industrialAccidentInsurance;
        private final BigDecimal incomeTax;
        private final BigDecimal localIncomeTax;
        private final BigDecimal money;

        public Builder(Member member, double money, MoneyHistoryRequest request) {
            this.member = member;
            this.moneyYear = String.valueOf(request.getMoneyYear());
            this.moneyMonth = request.getMoneyMonth();
            this.moneyType = request.getMoneyType();
            this.beforeMoney = CommonFormat.convertDoubleToDecimal(request.getBeforeMoney());
            this.nationalPension = CommonFormat.convertDoubleToDecimal(request.getNationalPension());
            this.healthInsurance = CommonFormat.convertDoubleToDecimal(request.getHealthInsurance());
            this.longTermCareInsurance = CommonFormat.convertDoubleToDecimal(request.getLongTermCareInsurance());
            this.employmentInsurance = CommonFormat.convertDoubleToDecimal(request.getEmploymentInsurance());
            this.industrialAccidentInsurance = CommonFormat.convertDoubleToDecimal(request.getIndustrialAccidentInsurance());
            this.incomeTax = CommonFormat.convertDoubleToDecimal(request.getIncomeTax());
            this.localIncomeTax = CommonFormat.convertDoubleToDecimal(request.getLocalIncomeTax());
            this.money = CommonFormat.convertDoubleToDecimal(money).add(nationalPension.add(healthInsurance).add(longTermCareInsurance).add(employmentInsurance).add(industrialAccidentInsurance).add(incomeTax).add(localIncomeTax));
        }

        @Override
        public MoneyHistory build() {
            return new MoneyHistory(this);
        }
    }
}
