package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 4)
    private String attendanceYear;

    @Column(nullable = false, length = 2)
    private String attendanceMonth;

    @Column(nullable = false, length = 2)
    private String attendanceDay;

    @Column(nullable = false)
    private LocalTime timeAttendance;

    private LocalTime timeLeaveWork;

    public void leaveWork() {
        this.timeLeaveWork = LocalTime.now();
    }

    private Attendance(Builder builder) {
        this.member = builder.member;
        this.attendanceYear = builder.attendanceYear;
        this.attendanceMonth = builder.attendanceMonth;
        this.attendanceDay = builder.attendanceDay;
        this.timeAttendance = builder.timeAttendance;
    }

    public static class Builder implements CommonModelBuilder<Attendance> {

        private final Member member;
        private final String attendanceYear;
        private final String attendanceMonth;
        private final String attendanceDay;
        private final LocalTime timeAttendance;

        public Builder(Member member) {
            this.member = member;
            this.attendanceYear = String.valueOf(LocalDate.now().getYear());
            this.attendanceMonth = LocalDate.now().getMonthValue() < 10 ? "0" + LocalDate.now().getMonthValue() : String.valueOf(LocalDate.now().getMonthValue());
            this.attendanceDay = LocalDate.now().getDayOfMonth() < 10 ? "0" + LocalDate.now().getDayOfMonth() : String.valueOf(LocalDate.now().getDayOfMonth());
            this.timeAttendance = LocalTime.now();
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}