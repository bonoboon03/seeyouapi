package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.money.MoneyRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Money {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private BigDecimal beforeMoney;

    @Column(nullable = false)
    private BigDecimal nationalPension;

    @Column(nullable = false)
    private BigDecimal healthInsurance;

    @Column(nullable = false)
    private BigDecimal longTermCareInsurance;

    @Column(nullable = false)
    private BigDecimal employmentInsurance;

    @Column(nullable = false)
    private BigDecimal industrialAccidentInsurance;

    @Column(nullable = false)
    private BigDecimal incomeTax;

    @Column(nullable = false)
    private BigDecimal localIncomeTax;

    public void putMoney(MoneyRequest moneyRequest) {
        this.moneyType = moneyRequest.getMoneyType();
        this.beforeMoney = CommonFormat.convertDoubleToDecimal(moneyRequest.getBeforeMoney());
        this.nationalPension = CommonFormat.convertDoubleToDecimal(moneyRequest.getNationalPension());
        this.healthInsurance = CommonFormat.convertDoubleToDecimal(moneyRequest.getHealthInsurance());
        this.longTermCareInsurance = CommonFormat.convertDoubleToDecimal(moneyRequest.getLongTermCareInsurance());
        this.employmentInsurance = CommonFormat.convertDoubleToDecimal(moneyRequest.getEmploymentInsurance());
        this.industrialAccidentInsurance = CommonFormat.convertDoubleToDecimal(moneyRequest.getIndustrialAccidentInsurance());
        this.incomeTax = CommonFormat.convertDoubleToDecimal(moneyRequest.getIncomeTax());
        this.localIncomeTax = CommonFormat.convertDoubleToDecimal(moneyRequest.getLocalIncomeTax());
    }

    private Money(Builder builder) {
        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
    }

    public static class Builder implements CommonModelBuilder<Money> {
        private final Member member;
        private final MoneyType moneyType;
        private final BigDecimal beforeMoney;
        private final BigDecimal nationalPension;
        private final BigDecimal healthInsurance;
        private final BigDecimal longTermCareInsurance;
        private final BigDecimal employmentInsurance;
        private final BigDecimal industrialAccidentInsurance;
        private final BigDecimal incomeTax;
        private final BigDecimal localIncomeTax;

        public Builder(Member member, MoneyRequest request) {
            this.member = member;
            this.moneyType = request.getMoneyType();
            this.beforeMoney = CommonFormat.convertDoubleToDecimal(request.getBeforeMoney());
            this.nationalPension = CommonFormat.convertDoubleToDecimal(request.getNationalPension());
            this.healthInsurance = CommonFormat.convertDoubleToDecimal(request.getHealthInsurance());
            this.longTermCareInsurance = CommonFormat.convertDoubleToDecimal(request.getLongTermCareInsurance());
            this.employmentInsurance = CommonFormat.convertDoubleToDecimal(request.getEmploymentInsurance());
            this.industrialAccidentInsurance = CommonFormat.convertDoubleToDecimal(request.getIndustrialAccidentInsurance());
            this.incomeTax = CommonFormat.convertDoubleToDecimal(request.getIncomeTax());
            this.localIncomeTax = CommonFormat.convertDoubleToDecimal(request.getLocalIncomeTax());
        }

        @Override
        public Money build() {
            return new Money(this);
        }
    }
}
