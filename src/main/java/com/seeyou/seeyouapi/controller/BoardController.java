package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.board.BoardItem;
import com.seeyou.seeyouapi.model.board.BoardResponse;
import com.seeyou.seeyouapi.model.board.BoardRequest;
import com.seeyou.seeyouapi.model.board.BoardSearchRequest;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.service.BoardService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/data")
    public CommonResult setBoard(@RequestBody @Valid BoardRequest request) {
        boardService.setBoard(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 리스트")
    @GetMapping("/all/{page}")
    public ListResult<BoardItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchTitle", required = false) String searchTitle
            ) {
        BoardSearchRequest request = new BoardSearchRequest();
        request.setTitle(searchTitle);
        return ResponseService.getListResult(boardService.getList(page, request), true);
    }

    @ApiOperation(value = "게시글 리스트 상세")
    @GetMapping("/board")
    public SingleResult<BoardResponse> getBoard(@RequestParam("id") int id) {
        return ResponseService.getSingleResult(boardService.getBoard(id));
    }


    @ApiOperation(value = "게시글 수정")
    @PutMapping("/{id}")
    public CommonResult putBoard(@PathVariable long id, @RequestBody @Valid BoardRequest request) {
        boardService.putBoard(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delBoard(@PathVariable long id) {
        boardService.delBoard(id);
        return ResponseService.getSuccessResult();
    }
}