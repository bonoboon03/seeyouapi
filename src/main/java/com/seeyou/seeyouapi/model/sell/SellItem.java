package com.seeyou.seeyouapi.model.sell;

import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long sellId;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "판매일")
    private String dateSell;

    @ApiModelProperty(notes = "환불일")
    private String dateRefund;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private SellItem(Builder builder) {
        this.sellId = builder.sellId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.dateSell = builder.dateSell;
        this.dateRefund = builder.dateRefund;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<SellItem> {
        private final Long sellId;
        private final String productName;
        private final Integer quantity;
        private final BigDecimal price;
        private final String dateSell;
        private final String dateRefund;
        private final String isComplete;

        public Builder(Sell sell) {
            this.sellId = sell.getId();
            this.productName = sell.getSellProduct().getProductName();
            this.quantity = sell.getQuantity();
            this.price = sell.getPrice();
            this.dateSell = sell.getSellYear() + "-" + sell.getSellMonth() + "-" + sell.getSellDay() + " " + sell.getTimeSell().toString();
            this.dateRefund = sell.getDateRefund() == null ? "-" : sell.getDateRefund().toString();
            this.isComplete = sell.getIsComplete() ? "O" : "X";
        }

        @Override
        public SellItem build() {
            return new SellItem(this);
        }
    }
}
