package com.seeyou.seeyouapi.model.board;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardSearchRequest {
    private String title;
}
