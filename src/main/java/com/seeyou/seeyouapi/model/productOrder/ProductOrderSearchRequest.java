package com.seeyou.seeyouapi.model.productOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductOrderSearchRequest {
    private String orderYear;
    private String orderMonth;
    private String orderDay;
}
