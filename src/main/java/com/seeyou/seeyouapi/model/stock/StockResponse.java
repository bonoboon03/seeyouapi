package com.seeyou.seeyouapi.model.stock;

import com.seeyou.seeyouapi.entity.Stock;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StockResponse {
    @ApiModelProperty(notes = "상품")
    private String productName;

    @ApiModelProperty(notes = "재고수량")
    private Integer stockQuantity;

    @ApiModelProperty(notes = "최소기준수량")
    private Integer minQuantity;


    private StockResponse(Builder builder) {
        this.productName = builder.productName;
        this.stockQuantity = builder.stockQuantity;
        this.minQuantity = builder.minQuantity;
    }


    public static class Builder implements CommonModelBuilder<StockResponse> {
        private final String productName;
        private final Integer stockQuantity;
        private final Integer minQuantity;

        public Builder(Stock stock) {
            this.productName = stock.getProduct().getProductName();
            this.stockQuantity = stock.getStockQuantity();
            this.minQuantity = stock.getMinQuantity();
        }
        @Override
        public StockResponse build() {
            return new StockResponse(this);
        }
    }
}
