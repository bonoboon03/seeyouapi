package com.seeyou.seeyouapi.model.product;

import com.seeyou.seeyouapi.enums.ProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSearchRequest {
    private ProductType productType;
    private String productName;
}
