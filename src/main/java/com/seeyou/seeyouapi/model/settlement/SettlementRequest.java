package com.seeyou.seeyouapi.model.settlement;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SettlementRequest {
    @ApiModelProperty(notes = "년도", required = true)
    @NotNull
    private Integer settlementYear;

    @ApiModelProperty(notes = "월", required = true)
    @NotNull
    private String settlementMonth;
}
