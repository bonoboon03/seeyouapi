package com.seeyou.seeyouapi.model.settlement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SettlementSearchRequest {
    private String settlementYear;
    private String settlementMonth;
}
